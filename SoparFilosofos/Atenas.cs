﻿using System;
using System.Threading;

namespace SoparFilosofos
{
    public class Filosofo
    {
        private int Id { get; set; }
        public Palillo[] Palillos { get; set; }
        private int Linea { get; set; }

        private readonly string[] Filosofia = new string[]
        {
            "¿Por qué el efecto causa-efecto causa efecto?",
            "AGARRA A UNA MUJER, AGARRALA Y LANZALA",
            "Porque el efecto causa-efecto es a su vez causa y efecto.",
            "AGUANTA GATITO, AGUANTA",
            "Remember salpepper",
            "Yo en vez de corazón tengo un gato, que da golpes porque quiere comer.",
            "Perdió su vida en el LoL, ya es demasiado tarde para una solucióoon",
            "¡DINAMÓ!",
            "Disculpe señora vaca pero nonono nono nonono noo noo",
            "AHHH MIS OJOS MIS OJOS... ah ya está.",
            "Disculpe profesor Snape, tengo una pequeña duda mi varita es de plastico PVC ¿USTED LA CONGRATULA?"
        };
        public bool Comiendo = true;
        private readonly Random rnd = new Random();

        public Filosofo(int id, int linea, ref Palillo izquierdo, ref Palillo derecho)
        {
            this.Id = id;
            this.Linea = linea;
            this.Palillos = new Palillo[2] { izquierdo, derecho };
        }

        public bool TienePalillos()
        {
            return (this.Palillos[0].Usando == false & this.Palillos[1].Usando == false);
        }
        public void Comer()
        {
            Program.ClearConsoleLine(this.Linea);
            Console.SetCursorPosition(0, this.Linea);
            if (TienePalillos())
            {
                this.Palillos[0].Usando = true;
                this.Palillos[1].Usando = true;

                Console.WriteLine($"El filósofo {this.Id} come.");
                Thread.Sleep(this.rnd.Next(2000, 6000));
                
                this.Palillos[0].Usando = false;
                this.Palillos[1].Usando = false;
            }
            else Console.WriteLine($"El filósofo {this.Id} no encuentra los palillos!");
            Thread.Sleep(1000);

        }
        public void Pensar()
        {
            Program.ClearConsoleLine(this.Linea);
            Console.SetCursorPosition(0, this.Linea);
            Console.WriteLine($"El filósofo {this.Id} piensa: {this.Filosofia[this.rnd.Next(0, this.Filosofia.Length)]}");
            Thread.Sleep(this.rnd.Next(1000, 4000));
        }
        public void Ejecutar()
        {
            while (this.Comiendo)
            {
                Pensar();
                Comer();
            }
        }

    }
    public class Palillo
    {
        public int Id { get; set; }
        public bool Usando { get; set; }
        public Palillo(int id)
        {
            this.Id = id;
            this.Usando = false;
        }
    }
}