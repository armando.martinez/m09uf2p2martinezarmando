﻿using System;
using System.Threading;


namespace SoparFilosofos
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.SetCursorPosition(0, 1);
            Console.WriteLine("-- Aprieta cualquier tecla para terminar --");

            Palillo[] palillos = new Palillo[]
            {
                new Palillo(0),
                new Palillo(1),
                new Palillo(2),
                new Palillo(3),
                new Palillo(4)
            };

            Filosofo[] mesa = new Filosofo[]
            {
                new Filosofo(0, 3, ref palillos[4], ref palillos[0]),
                new Filosofo(1, 5, ref palillos[0], ref palillos[1]),
                new Filosofo(2, 7, ref palillos[1], ref palillos[2]),
                new Filosofo(3, 9, ref palillos[2], ref palillos[3]),
                new Filosofo(4, 11, ref palillos[3], ref palillos[4])
            };

            Thread[] threads = new Thread[5];

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(mesa[i].Ejecutar);
                threads[i].Start();
                Thread.Sleep(200);
            }

            Console.ReadKey();
            
            foreach (Filosofo f in mesa)
            {
                f.Comiendo = false;
            }
        }

        public static void ClearConsoleLine(int line)
        {
            Console.SetCursorPosition(0, line);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, line);
        }

    }
}
